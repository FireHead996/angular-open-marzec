import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { AuthService } from './core/auth/auth/auth.service';

@Component({
  selector: 'app-root, lubie-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'Szkolenie Angular';

  user = { name: 'Guest' };

  time = ''

  constructor(
    public auth:AuthService,
    private cdr: ChangeDetectorRef) {
    // cdr.detach()

    // setInterval(() => {
    //   // cdr.detectChanges()
    //   this.time = new Date().toLocaleTimeString()
    // }, 1000)
  }

  login() {
    // this.user = { name: 'Admin' }
    this.auth.login()

    // this.cdr.detectChanges()
  }

  logout(){
    this.auth.logout()
  }

  getTime() {
    console.log('getTIme')
    // return new Date().toLocaleTimeString()
    // return Math.random() // ExpressionChangedAfterItHasBeenCheckedError
    // https://angular.io/errors/NG0100
    return this.time
  }
}
