import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistViewComponent } from './containers/playlist-view/playlist-view.component';

const routes: Routes = [
  {
    path:'playlists',
    children:[
      {
        path: '',
        component: PlaylistViewComponent
      }, 
      {
        path: ':playlist_id',
        component: PlaylistViewComponent
      }
    ]
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
