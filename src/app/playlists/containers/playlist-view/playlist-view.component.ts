import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, map, share, switchMap, tap } from 'rxjs/operators';
import { Playlist } from 'src/app/core/model/playlist';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

@Component({
  selector: 'app-playlist-view',
  templateUrl: './playlist-view.component.html',
  styleUrls: ['./playlist-view.component.scss']
})
export class PlaylistViewComponent implements OnInit {

  mode: 'edit' | 'details' | 'create' = 'details'

  playlists = this.service.getPlaylists()

  selected?: Observable<Playlist | undefined>

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService) { }

  private updateSelected() {
    // switch all AsyncPipes to new Observable to resubscribe
    this.selected = this.route.paramMap.pipe(
      map(param => param.get('playlist_id')),
      filter(id => !!id),
      switchMap(id => this.service.getPlaylistById(id!)),
      share()
    );
  }

  ngOnInit(): void {
    this.updateSelected()
  }

  selectPlaylistById(playlist_id: Playlist['id']) {
    this.router.navigate(['/playlists', playlist_id], {})
  }

  edit() {
    this.mode = 'edit'
  }

  cancel() {
    this.mode = 'details'
  }

  save(draft: Playlist) {
    this.mode = 'details'
    this.service.savePlaylist(draft).subscribe((saved) => {
      this.selectPlaylistById(saved.id)
      this.updateSelected()
      this.playlists = this.service.getPlaylists()
    })
  }

  createPlaylist() {
    this.mode = 'create'
    this.selected = of({} as Playlist)
  }

  deletePlaylist(id: Playlist['id']) {

    this.service.deletePlaylistById(id).subscribe(() => {
      this.selectPlaylistById('')
      this.selected = of();
      this.playlists = this.service.getPlaylists()
    })
  }

}
