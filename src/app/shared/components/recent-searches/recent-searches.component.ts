import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss']
})
export class RecentSearchesComponent implements OnInit {
  queries: string[] = []

  constructor(
    private searchService: MusicSearchService
  ) {

    searchService.queryChanges.subscribe(query => {
      console.log(query)
      this.queries.push(query)
      this.queries = this.queries.slice(-3)
    })
  }

  search(query: string) {
    this.searchService.searchAlbums(query)
  }

  ngOnInit(): void {

  }

}
