import { Directive, ElementRef, EventEmitter, HostListener, Optional } from '@angular/core';
import { ToggleContainerDirective } from './toggle-container.directive';

@Directive({
  selector: '[appToggleTrigger]',
  exportAs: 'appToggleTrigger',
  // host:{
  //   '(click)':'toggle($event)'
  // }
})
export class ToggleTriggerDirective {

  toggled = new EventEmitter()

  @HostListener('click', ['$event'])
  toggle(event: MouseEvent) {
    this.toggled.emit()
  }

  constructor(public elem: ElementRef<HTMLElement>,
    @Optional() private parent: ToggleContainerDirective) {

    if (this.parent) {
      this.parent.appToggleTriggerDirective = this;
    }
    // this.elem.nativeElement.onclick = () => {
    //   this.toggled.emit()
    // }
  }

}
