import { Directive } from '@angular/core';
import { ToggleCollapsibleDirective } from './toggle-collapsible.directive';
import { ToggleTriggerDirective } from './toggle-trigger.directive';

@Directive({
  selector: '[appToggleContainer]'
})
export class ToggleContainerDirective {
  appToggleTriggerDirective?:ToggleTriggerDirective

  constructor() { }

}
