import { Component, OnInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  children: TabComponent[] = []

  toggleChild(activeTab: TabComponent) {
    this.children.forEach(tab => {
      if (activeTab === tab) {
        tab.open = true
        this.selected = tab.title
      } else {
        tab.open = false
      }
    })
  }

  unregister(tab: TabComponent) {
    const index = this.children.indexOf(tab)
    this.children.splice(index, 1)
  }

  register(tab: TabComponent) {
    this.children.push(tab)
  }

  selected: any

  constructor() { }

  ngOnInit(): void {
  }

}
