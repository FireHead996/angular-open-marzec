import { Component, Input, OnInit } from '@angular/core';
import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {

  @Input() title?: string;

  open = false

  constructor(private parent: TabsComponent) {

  }

  ngOnInit(): void {
    this.parent.register(this)
  }
  
  toggle() {
    this.parent.toggleChild(this)
    // this.open = !this.open
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

    this.parent.unregister(this)
  }
  
}
