import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { YesNoPipe } from './pipes/yesno.pipe';
import { TabsModule } from './tabs/tabs.module';
import { ToggleModule } from './toggle/toggle.module';
import { RecentSearchesComponent } from './components/recent-searches/recent-searches.component';

@NgModule({
  declarations: [CardComponent, YesNoPipe, RecentSearchesComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule,
    ToggleModule,
  ],
  exports: [
    CardComponent, 
    FormsModule, 
    YesNoPipe, 
    TabsModule,
    ToggleModule,    
    ReactiveFormsModule, RecentSearchesComponent,
  ]
})
export class SharedModule { }
