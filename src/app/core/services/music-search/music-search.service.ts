import { EventEmitter, Inject, Injectable } from '@angular/core';
import { Album, AlbumsSearchResponse } from '../../model/search';
import { INITIAL_SEARCH_RESULTS, SEARCH_API_URL } from '../../tokens';

import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { BehaviorSubject, concat, EMPTY, from, merge, NEVER, Observable, of, ReplaySubject, Subject, Subscription, throwError } from 'rxjs';
import { AuthService } from '../../auth/auth/auth.service';

import { catchError, concatAll, concatMap, exhaust, exhaustMap, map, mergeAll, mergeMap, pluck, retryWhen, startWith, switchAll, switchMap, tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {
  private results = new BehaviorSubject<Album[]>(this.initial);
  readonly resultsChange = this.results.asObservable()

  private queries = new ReplaySubject<string>(3/* ,10_000 */)
  readonly queryChanges = this.queries.asObservable()

  private message = new Subject<string>()
  readonly messageChanges = this.message.asObservable()


  constructor(
    private http: HttpClient,
    @Inject(INITIAL_SEARCH_RESULTS) private initial: Album[],
    @Inject(SEARCH_API_URL) private api_url: string) {
    ; (window as any).subject = this.results

    this.queries
      .pipe(
        switchMap(query =>
          this.fetchSearchResults(query).pipe(
            catchError(error => {
              this.message.next(error.message)
              return NEVER
            })
          )),
      )
      .subscribe(this.results)
  }

  searchAlbums(query: string) {
    this.queries.next(query)
  }


  fetchSearchResults(query: string) {
    return this.http.get<AlbumsSearchResponse>(this.api_url, {
      params: {
        type: 'album',
        q: query
      },
    }).pipe(
      map(res => res.albums.items),
      // tap(albums => this.results.next(albums))
    );
  }

  fetchAlbumById(id: string) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${id}`)
  }
}

