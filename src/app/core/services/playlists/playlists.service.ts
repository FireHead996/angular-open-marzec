import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, mapTo, switchMap } from 'rxjs/operators';
import { AuthService } from '../../auth/auth/auth.service';
import { Playlist } from '../../model/playlist';
import { PagingObject } from '../../model/search';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {


  constructor(private http: HttpClient, public auth: AuthService) { }

  deletePlaylistById(id: string) {
    return this.http.delete(`https://api.spotify.com/v1/playlists/${id}/followers`)
  }

  savePlaylist(draft: Playlist) {
    type PlaylistDraft = Pick<Playlist, 'name' | 'public' | 'description'>;

    if (draft.id) { // UPDATE
      return this.http.put(`https://api.spotify.com/v1/playlists/${draft.id}`, {
        name: draft.name,
        public: draft.public,
        description: draft.description
      } as PlaylistDraft).pipe(mapTo(draft))

    } else { // CREATE NEW
      return this.auth.getUser().pipe(
        switchMap(user => this.http.post<Playlist>(`https://api.spotify.com/v1/users/${user!.id}/playlists`, {
          name: draft.name,
          public: draft.public,
          description: draft.description
        } as PlaylistDraft))
      )
    }
  }

  getPlaylists(): Observable<Playlist[]> {
    return this.auth.getUser().pipe(
      switchMap(user => this.http.get<PagingObject<Playlist>>(`https://api.spotify.com/v1/users/${user!.id}/playlists`)),
      map(res => res.items)
    )
  }

  getPlaylistById(playlist_id: string): Observable<Playlist | undefined> {
    return this.http.get<Playlist>(`https://api.spotify.com/v1/playlists/${playlist_id}`)
  }

}
