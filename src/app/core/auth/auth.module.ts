import { Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './auth/auth.service';

import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthInterceptor } from './auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
        allowedUrls:[
          'https://api.spotify.com/v1/'
        ],
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ]
})
export class AuthModule {
  constructor(private auth: AuthService, @Inject(HTTP_INTERCEPTORS) interceptors: any) {
    auth.init()
    // console.log(interceptors)
  }

}
