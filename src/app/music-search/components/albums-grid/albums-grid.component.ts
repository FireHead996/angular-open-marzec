import { Component, Input, OnInit } from '@angular/core';
import { Album } from 'src/app/core/model/search';

@Component({
  selector: 'app-albums-grid',
  templateUrl: './albums-grid.component.html',
  styleUrls: ['./albums-grid.component.scss']
})
export class AlbumsGridComponent implements OnInit {

  @Input() albums: Album[] | null = []

  constructor() { }

  ngOnInit(): void {
  }

}
