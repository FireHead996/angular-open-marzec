import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';

/* /search... */
const routes: Routes = [
  {
    path: '',
    component: AlbumSearchComponent
  },
  {
    path: '/albums',
    component: AlbumDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule { }
