import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, ConnectableObservable, Observable, ReplaySubject, Subject, Subscription } from 'rxjs';
import { multicast, publish, publishBehavior, publishReplay, refCount, share, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Album } from 'src/app/core/model/search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';


@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit /*  extends BaseComponent */ {

  message = this.service.messageChanges
  query = this.service.queryChanges
  // results = this.service.resultsChange

  albums: Album[] = []

  results?: Observable<Album[]> = this.service.resultsChange.pipe(
    share()
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService) {

    // const q = route.snapshot.queryParamMap.get('q')
    
    route.queryParamMap.subscribe(queryParamMap => {
      const q = queryParamMap.get('q')
      if (q) {
        this.service.searchAlbums(q)
      }
    })
  }

  submitSearchQuery(query: string = 'batman') {
    // this.service.searchAlbums(query)

    this.router.navigate(['/search'], {
      queryParams: {
        q: query
      }
    })
  }

  ngOnInit(): void {
  }

}
