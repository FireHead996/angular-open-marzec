import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { iif, NEVER } from 'rxjs';
import { catchError, filter, map, mergeAll, mergeMap, switchMap } from 'rxjs/operators';
import { Album, Track } from 'src/app/core/model/search';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  @ViewChild('audioRef', { static: false })
  audioRef?: ElementRef<HTMLAudioElement>

  // album?: Album
  currentTrack?: Track

  playTrack(track: Track) {
    this.currentTrack = track

    setTimeout(() => {
      if (this.audioRef) {
        this.audioRef.nativeElement.volume = 0.2;
        this.audioRef.nativeElement.play()
       // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio
      }
    })
  }

  albumObs = this.route.queryParamMap.pipe(
    map(queryParamMap => queryParamMap.get('id')),
    switchMap(id => iif(
      () => id !== null,
      this.service.fetchAlbumById(id!),
      NEVER
    )),
    catchError(err => { this.message = err.message; return [] }),
    // mergeAll(),
    // o => o
  )
  message = '';

  constructor(
    private route: ActivatedRoute,
    private service: MusicSearchService) {

    // this.route.queryParamMap.subscribe(queryParamMap => {
    //   const id = queryParamMap.get('id')

    //   if (!id) { return }
    //   this.service.fetchAlbumById(id).subscribe(album => {
    //     this.album = album
    //   })
    // })


  }

  ngOnInit(): void {

  }

}
