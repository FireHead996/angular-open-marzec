import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SandboxRoutingModule } from './sandbox-routing.module';
import { SandboxComponent } from './containers/sandbox/sandbox.component';
import { SharedModule } from '../shared/shared.module';
import { INITIAL_SEARCH_RESULTS } from '../core/tokens';
import { Album } from '../core/model/search';

// const mockAlbums: Partial<Album>[] = [
  const mockAlbums: Pick<Album, 'id' | 'name' | 'type' | 'images'>[] = [
    {
      id: '123', type: 'album', name: 'Service Album 123', images: [
        { width: 300, height: 300, url: 'https://www.placecage.com/c/300/300' }
      ]
    },
    {
      id: '234', type: 'album', name: 'Service Album 234', images: [
        { width: 300, height: 300, url: 'https://www.placecage.com/c/400/400' }
      ]
    },
    {
      id: '345', type: 'album', name: 'Service Album 345', images: [
        { width: 300, height: 300, url: 'https://www.placecage.com/c/800/800' }
      ]
    },
    {
      id: '456', type: 'album', name: 'Service Album 456', images: [
        { width: 300, height: 300, url: 'https://www.placecage.com/c/600/600' }
      ]
    },
  ]

@NgModule({
  declarations: [SandboxComponent],
  imports: [
    CommonModule,
    SandboxRoutingModule,
    SharedModule
  ],
  providers: [
    {
      provide: INITIAL_SEARCH_RESULTS,
      useValue: mockAlbums
    }
  ]
})
export class SandboxModule { }

