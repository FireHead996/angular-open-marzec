# Git + install
cd ..
git clone https://bitbucket.org/ev45ive/angular-open-marzec.git nazwa_katalogu
cd nazwa_katalogu
npm install 
npm run ng s


## VSCode + Chrome
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://augury.rangle.io/

$ node -v
v14.15.4

$ npm -v
6.14.10

$ code -v
1.40.1
8795a9889db74563ddd43eb0a897a2384129a619
x64

$ git --version
git version 2.29.2.windows.3

npm i -g @angular/cli
ng version
Angular CLI: 11.2.5
ng help

## Create new project
cd ..
ng new --strict --routing --style scss angular-open-marzec

## RUn dev server
cd angular-open-marzec
ng s -o 

## Semver
https://semver.org/lang/pl/
https://semver.npmjs.com/


## Lint
https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode


## Modules , Schematics
https://angular.io/guide/styleguide#lift

ng g c -it -is --flat --skip-tests  playlists/components/playlist-list-item 


ng g m playlists -m app --routing

ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-form

ng g c playlists/containers/playlist-view --export

ng g m shared -m playlists
ng g c shared/components/card --export


## Bootstrap
https://ng-bootstrap.github.io/ 

npm install bootstrap


## Ng-template microsyntax
https://ng-bootstrap.github.io/#/components/modal/examples
https://material.angular.io/components/table/overview


## TypeScript
ng g m core -m app
ng g i core/model/playlist


## Projection
ng g c shared/components/card --export


## Pipes
ng g p shared/pipes/yesno --export

## Tabs / Accordions
ng g m sandbox -m app --routing
ng g c sandbox/containers/sandbox

ng g m shared/tabs -m shared 
ng g c shared/tabs/components/tab --export 
ng g c shared/tabs/components/tabs --export 

## Directives
ng g m shared/toggle -m shared
ng g d shared/toggle/toggle-container --export 
ng g d shared/toggle/toggle-trigger --export 
ng g d shared/toggle/toggle-collapsible  --export 


## Music Search

ng g m music-search -m app --routing
ng g c music-search/containers/album-search

ng g c music-search/components/search-form
ng g c music-search/components/albums-grid
ng g c music-search/components/album-card

ng g i core/model/search


## Services
ng g s core/services/music-search --flat false

## Auth
https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow
https://github.com/manfredsteyer/angular-oauth2-oidc


ng g m core/auth -m core
ng g s core/auth/auth

## RxJS
https://rxmarbles.com/#of
https://rxjs-dev.firebaseapp.com/guide/overview
https://rxviz.com/examples/basic-interval


## Intereceptors
ng g interceptor core/auth/auth


## Recent search
ng g c shared/components/recent-searches --export

## Playlists

ng g s core/services/playlists